const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.

//first 
function keys(obj) {
  // Retrieve all the names of the object's properties.
  // Return the keys as strings in an array.
  // Based on http://underscorejs.org/#keys
  let keysArry= [];
  for(const attribute in obj)
  {
    keysArry.push( attribute);
  }
  return keysArry;
}



function values(obj) {
  // Return all of the values of the object's own properties.
  // Ignore functions
  // http://underscorejs.org/#values

  let keysArry= [];
  for(const attribute in obj)
  {
    keysArry.push(obj [attribute]);
  }
  return keysArry;
}


// third one

function mapObject(obj, cb) {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
  // http://underscorejs.org/#mapObject

for(const attribute in obj)
{
    obj [attribute]  = cb(obj[attribute])
}
} 

// fourth one


function pairs(obj) {
  // Convert an object into a list of [key, value] pairs.
  // http://underscorejs.org/#pairs
  let arrayPairs = [];

for(const attribute in obj)
{
   arrayPairs.push([attribute,obj[attribute]])
}
return arrayPairs;
}

/* STRETCH PROBLEMS */
// function 5

function invert(obj) {
  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // http://underscorejs.org/#invert
  let arrayPairs = [];

  for(const attribute in obj)
  {
     arrayPairs.push([attribute,obj[attribute]])
  }
  
  let newObject = { 
  }
  for(let index = 0;index <arrayPairs.length;index++)
  {
    newObject[arrayPairs[index][1]] = arrayPairs[index][0];  
  }
  return newObject
  }


  //function 6
  function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
      for(const attribute in defaultProps)
          {
            if(obj [attribute] === undefined)
            {
                obj[attribute] = defaultProps[attribute];
            }
  
            }
            return obj;
  }