const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code. 
/*
  Complete the following functions.
  These functions only need to work with arrays.
  A few of these functions mimic the behavior of the `Built` in JavaScript Array Methods.
  The idea here is to recreate the functions from scratch BUT if you'd like,
  feel free to Re-use any of your functions you build within your other functions.
  **DONT** Use for example. .forEach() to recreate each, and .map() to recreate map etc.
  You CAN use concat, puitems.forEach(callBack);list of elements, yielding each in turn to the `cb` function.

  This only needs to work with arrays.
  You should also pass the index into `cb` as the second argument
  based off http://underscorejs.org/#each*/

  //1st
function callBack(currrentValue,index,totalArry)
 {
        console.log("the element is "+currrentValue);
 }

  function each(elements, cb) {
      
        for(let index =0;index < elements.length;index++)
        {  
         cb(elements[index],index,elements);
        }


}
  
// each(items,callBack);
// each(item ,set) ;
//each(items,callBack);

//2 nd fuction
// function callBack(currrentValue,index,totalArry)
//  {
//         return currrentValue*currrentValue;
//  }


function map(elements, cb) {
 // Do NOT use .map, to complete this function.
 // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
 // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
 // Return the new array.


const Newarray=[];

 for(let index=0;index<elements.length;index++)
 {   

  Newarray.push(callBack(elements[index],index,elements));
 }
 return Newarray;
 
}

// let updatedArray = map(items,callBack);

//  console.log(updatedArray);


// 3function
  
//  function callBack(accumlator,currrentValue,index,totalArry)
//  {   
//     return  accumlator+currrentValue;
//  }

 function reduce(elements, cb, startingValue) {
  // Do NOT use .reduce to complete this function.
  // How reduce works: A reduce function combines all elements into a single value going from left to right.
  // Elements will be passed one by one into `cb` along with the `startingValue`.
  // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
  // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
  let initalValue =0
   let index;  
  if(startingValue===undefined)
  {
      index = 1;
      initalValue = elements[0];
  }
  else
  {
      index =0;
      initalValue = startingValue;
  }
  for(index ;index<elements.length;index++)
  {
     initalValue =  callBack(initalValue,items[index],index,items);
  }
  return initalValue;
}
// let value = reduce(items,callBack,-80);
// console.log(value);


   //  function 4
 
// function callBack(element ,index, array)
//  {
//      if(element>10)
//      return true;
//  }



function find(elements, cb){
  // Do NOT use .includes, to complete this function.
  // Look through each value in `elements` and pass each element to `cb`.
  // If `cb` returns `true` then return that element.
  // Return `undefined` if no elements pass the truth test.
  for(let index = 0;index<elements.length;index++)
  {
    
      let flag=callBack(elements[index],index,elements);
      if(flag === true)
      {
          return elements[index];
      }
  }
}


//  function 5

// function callBack(element,index,totalArry)
// {   
//     if(element>8)
//    return  true;
//    else{
//        false;
//    }
// }


function filter(elements, cb) {
 // Do NOT use .filter, to complete this function.
  // Similar to `find` but you will return an array of all elements that passed the truth test
  // Return an empty array if no elements pass the truth test
  // const Newarray=[];
 
  for(let index =0;index<elements.length;index++)
  {

  if(true ===callBack(elements[index],index,elements))
  {
      Newarray.push(elements[index]);
  }
  }
  return Newarray;
}
// let filterdArray =filter(items,callBack);
// console.log(filterdArray);

// function 6

const arr = [1, [2], [[3]], [[[4]]]];

// let filterdArray =filter(items,callBack);
// console.log(filterdArray);
function flatToInteger(source,destination)
{
    for(let index =0;index<source.length;index++)
    { 
        if( Array.isArray( source[index]) )
        {
            flatToInteger(source[index],destination);
        }
        else
        {
            destination.push(source[index]);
        }

    }
}

function flatten(elements) {
   let destination = [];
   
  for(let index =0;index< elements.length;index++)
  {  
      if(Array.isArray( elements[index]) )
    {
      flatToInteger(elements[index],destination);
   
    }
    else
    {
        destination.push(elements[index]);
    }
  }
  return destination;
}


let destination =flatten(arr);
console.log(destination);